package com.klym.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApplicationKlymApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApplicationKlymApplication.class, args);
    }

}
