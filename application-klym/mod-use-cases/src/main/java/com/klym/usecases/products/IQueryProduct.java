package com.klym.usecases.products;

import com.klym.domain.response.Confirmation;

public interface IQueryProduct {
    Confirmation queryProductsByCurrency(String currency);
}
