package com.klym.domain.product;

import lombok.Data;

@Data
public class ProductValue {

    private String currency;

    private Integer amount;
}
