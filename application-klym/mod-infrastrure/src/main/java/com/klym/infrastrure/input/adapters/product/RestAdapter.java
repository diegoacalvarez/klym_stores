package com.klym.infrastrure.input.adapters.product;

import com.klym.domain.product.Product;
import com.klym.domain.response.Confirmation;
import com.klym.usecases.products.ICreateProduct;
import com.klym.usecases.products.IQueryProduct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("product")
public class RestAdapter {

    @Autowired
    private ICreateProduct createProduct;

    @Autowired
    private IQueryProduct queryProduct;

    @PostMapping
    public Confirmation createProduct(@RequestBody Product product){
        return createProduct.create(product);
    }

    @GetMapping
    public Confirmation queryProductsByCurrency(@RequestParam String currency){
        return queryProduct.queryProductsByCurrency(currency);
    }
}
