package com.klym.domain.product;

import lombok.Data;

@Data
public class Product {

    private String name;

    private ProductValue value;
}
