package com.klym.usecases.products;

import com.klym.domain.product.Product;
import com.klym.domain.response.Confirmation;
import com.klym.domain.status.StatusEnum;
import com.klym.infrastrure.output.adapters.product.database.IMysqlJDBCAdapter;
import com.klym.infrastrure.output.adapters.product.restclient.IRestClientAdapter;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class QueryProductImpl implements IQueryProduct{

    @Autowired
    IMysqlJDBCAdapter mysqlJDBCAdapter;

    @Autowired
    IRestClientAdapter restClientAdapter;


    @Override
    public Confirmation queryProductsByCurrency(String currency) {
        List<Product> products = mysqlJDBCAdapter.queryProducts();
        Integer rate = restClientAdapter.queryExchangeRate(currency);

        return new Confirmation(StatusEnum.SUCCESS, calulateAmount(products, rate));
    }

    private List<Product> calulateAmount(List<Product> products, Integer rate) {
        for (int i = 0; i<products.size(); i++){
            products.get(i).getValue().setAmount(
                products.get(i).getValue().getAmount()
                    * rate);
        }
        return products;
    }
}
