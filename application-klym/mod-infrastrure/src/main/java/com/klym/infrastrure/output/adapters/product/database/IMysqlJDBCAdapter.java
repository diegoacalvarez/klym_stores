package com.klym.infrastrure.output.adapters.product.database;

import com.klym.domain.product.Product;
import com.klym.domain.response.Confirmation;

public interface IMysqlJDBCAdapter {
    Boolean insertProduct(Product product);

}
