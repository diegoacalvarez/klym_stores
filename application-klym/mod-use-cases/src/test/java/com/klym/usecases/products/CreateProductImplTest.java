package com.klym.usecases.products;

import com.klym.domain.product.Product;
import com.klym.domain.product.ProductValue;
import com.klym.domain.response.Confirmation;
import com.klym.domain.status.StatusEnum;
import com.klym.infrastrure.output.adapters.product.database.IMysqlJDBCAdapter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.test.util.ReflectionTestUtils;

class CreateProductImplTest {

    @Test
    public void createProductTest_success(){

        CreateProductImpl createProductImpl = new CreateProductImpl();
        Product product = new Product();
        product.setName("Licuadora");

        ProductValue productValue = new ProductValue();
        productValue.setAmount(100000);
        productValue.setCurrency("USD");

        product.setValue(productValue);

        Confirmation expectedConfirmation = new Confirmation(StatusEnum.SUCCESS);

        IMysqlJDBCAdapter mysqlJDBCAdapter = Mockito.mock(IMysqlJDBCAdapter.class);
        Mockito.when(mysqlJDBCAdapter.insertProduct(Mockito.any(Product.class))).thenReturn(true);

        ReflectionTestUtils.setField(createProductImpl, "mysqlJDBCAdapter", mysqlJDBCAdapter);
        Assertions.assertEquals(expectedConfirmation, createProductImpl.create(product));

    }

}