package com.klym.infrastrure.output.adapters.product.restclient;

public interface IRestClientAdapter {
    Integer queryExchangeRate(String currency);
}
