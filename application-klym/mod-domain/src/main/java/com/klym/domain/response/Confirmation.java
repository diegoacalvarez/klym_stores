package com.klym.domain.response;

import com.klym.domain.product.Product;
import com.klym.domain.status.StatusEnum;
import java.util.List;
import lombok.Data;

@Data
public class Confirmation {

    private StatusEnum status;

    private List<Product> response;

    public Confirmation(StatusEnum status) {
        this.status = status;
    }

    public Confirmation(StatusEnum status, List<Product> response) {
        this.status = status;
        this.response = response;
    }
}
