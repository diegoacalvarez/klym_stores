package com.klym.usecases.products;

import com.klym.domain.product.Product;
import com.klym.domain.response.Confirmation;

public interface ICreateProduct {
    Confirmation create(Product product);
}
