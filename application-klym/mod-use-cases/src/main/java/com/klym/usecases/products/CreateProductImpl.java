package com.klym.usecases.products;

import com.klym.domain.product.Product;
import com.klym.domain.response.Confirmation;
import com.klym.domain.status.StatusEnum;
import com.klym.infrastrure.output.adapters.product.database.IMysqlJDBCAdapter;
import org.springframework.stereotype.Component;

@Component
public class CreateProductImpl implements ICreateProduct {

    IMysqlJDBCAdapter mysqlJDBCAdapter;
    @Override
    public Confirmation create(Product product) {
        if(mysqlJDBCAdapter.insertProduct(product)){
            return new Confirmation(StatusEnum.SUCCESS);
        }
        return new Confirmation(StatusEnum.FAILED);
    }
}
